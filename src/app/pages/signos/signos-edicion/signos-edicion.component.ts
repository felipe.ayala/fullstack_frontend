
import { ActivatedRoute, Router, Params } from '@angular/router';
import { FormGroup, FormControl,FormBuilder } from '@angular/forms';
import { Signos } from './../../../_model/signos';
import { Paciente } from './../../../_model/paciente';
import { Component, OnInit } from '@angular/core';
import { SignosService } from '../../../_service/signos.service';
import { PacienteService } from '../../../_service/paciente.service';
import { Observable } from 'rxjs/internal/Observable';
import { map, startWith } from 'rxjs/operators';
import { MatDialog,MatSnackBar, MatDialogRef } from '@angular/material'; 
import { SignosPacienteEdicionComponent } from '../../signos/signos-paciente-edicion/signos-paciente-edicion.component';
import { DialogsService } from '../signos-paciente-edicion/dialogs.service';


@Component({
  selector: 'app-signos-edicion',
  templateUrl: './signos-edicion.component.html',
  styleUrls: ['./signos-edicion.component.css']
})
export class SignosEdicionComponent implements OnInit {


  myControlPaciente: FormControl = new FormControl();

  pacientes: Paciente[] = [];
  //idPacienteSeleccionado: number;
  filteredOptions: Observable<any[]>;

  pacienteSeleccionado: Paciente;
  id: number;
  signo: Signos;
  signos: Signos[] = [];
  form: FormGroup;
  edicion: boolean = false;
  isAdmin: boolean = false;
    maxFecha: Date = new Date() 
    public result: any;

  constructor(private builder: FormBuilder, private signosService: SignosService, private pacienteService : PacienteService,
     private route: ActivatedRoute, private router: Router,public snackBar: MatSnackBar,
     private dialog: MatDialog, private dialogsService: DialogsService) {
    this.signo = new Signos();

    console.log("1");
  }
 

  ngOnInit() {
    setTimeout(() => {
    //  this.isAdmin = this.securityService.esRoleAdmin();
    },1500);


    this.form = this.builder.group({
      'id': new FormControl(0),
      'paciente': this.myControlPaciente,
      'fecha': new FormControl(new Date()),
      'temperatura': new FormControl(''),
      'pulso': new FormControl(''),
      'ritmo': new FormControl('')
      
    }); 

    this.listarPacientes();
    this.route.params.subscribe((params: Params) => {
      this.id = params['id'];
      this.edicion = params['id'] != null;
      this.initForm();
    });
    
    
    this.filteredOptions = this.myControlPaciente.valueChanges.pipe(map(val => this.filter(val)));
  }

  filter(val: any) {
    if (val != null && val.idPaciente > 0) {
      return this.pacientes.filter(option =>
        option.nombres.toLowerCase().includes(val.nombres.toLowerCase()) || option.apellidos.toLowerCase().includes(val.apellidos.toLowerCase()) || option.dni.includes(val.dni));
    } else {
      return this.pacientes.filter(option =>
        option.nombres.toLowerCase().includes(val.toLowerCase()) || option.apellidos.toLowerCase().includes(val.toLowerCase()) || option.dni.includes(val));
    }
   
  }

  initForm(){
    if (this.edicion) {
      this.signosService.listarSignosPorId(this.id).subscribe(data => {
        let id = data.idSigno; 
       

        this.form = new FormGroup({
          'id': new FormControl(id),
          'temperatura': new FormControl(data.temperatura),
          'pulso': new FormControl(data.pulso),
          'fecha': new FormControl(new Date( data.fecha.toString() )),
          'ritmo': new FormControl(data.ritmo),
          'paciente': new FormControl(data.paciente)
        });
      });
    }
         
  }

  operar() {
    this.signo.idSigno = this.form.value['id'];
    this.signo.temperatura = this.form.value['temperatura'];
    this.signo.pulso = this.form.value['pulso'];
    this.signo.ritmo = this.form.value['ritmo'];
    this.signo.paciente = this.form.value['paciente'];
  
    var tzoffset = (new Date(this.form.value['fecha'])).getTimezoneOffset() * 60000; //offset in milliseconds
    var localISOTime = (new Date(this.form.value['fecha'] - tzoffset)).toISOString().slice(0, -1);
 
    this.signo.fecha = localISOTime; 

    if (this.signo != null && this.signo.idSigno > 0) {
      this.signosService.modificar(this.signo).subscribe(data => {
 
        this.snackBar.open("Se modificó", "Aviso", { duration: 2000 });

        setTimeout(() => {
          this.limpiaformulario();
        }, 2000); 

        this.signosService.listar().subscribe(signos => {
          this.signosService.signosCambio.next(signos);
          this.signosService.mensajeCambio.next('Se registró');
        });

      });
    } else {
      this.signosService.registrar(this.signo).subscribe(data => {
        this.snackBar.open("Se registró", "Aviso", { duration: 2000 });

        setTimeout(() => {
          this.limpiaformulario();
        }, 2000); 
         
        this.signosService.listar().subscribe(signos => {
          this.signosService.signosCambio.next(signos);
          this.signosService.mensajeCambio.next('Se registró');
        });

      });
    }
  
    this.router.navigate(['signos']);
  }

  limpiaformulario()
  {
    this.form = new FormGroup({
      'id': new FormControl(0),
     // 'fecha': new FormControl(''),
      'temperatura': new FormControl(''),
      'pulso': new FormControl(''),
      'ritmo': new FormControl(''),
      'paciente': new FormControl('')
    });

    this.pacienteSeleccionado=null;
    
  }

  displayFn(val: Paciente) {
    return val ? `${val.nombres} ${val.apellidos}` : val;
  }

  listarPacientes() {
    this.pacienteService.listar().subscribe(data => {
      this.pacientes = data;
    });
  }
  seleccionarPaciente(e: any){
    console.log(e.option.value);
    this.pacienteSeleccionado = e.option.value;
  }


  public openDialog() {
    //let dialogRef = this.dialog.open(SignosPacienteEdicionComponent, {
    //  width: '500px',
    //  disableClose: false
    //}).subscribe(res => this.pacientes= res);
    console.log("20");
    this.dialogsService
    .confirm('Confirm Dialog', 'Are you sure you want to do this?')
      .subscribe(res => this.result = res);
  }
 

}

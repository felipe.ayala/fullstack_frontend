import { ActivatedRoute } from '@angular/router';
import { Signos } from '../../_model/signos';
import { MatSort, MatPaginator, MatTableDataSource, MatSnackBar } from '@angular/material';
import { Component, OnInit, ViewChild } from '@angular/core';
import { SignosService } from '../../_service/signos.service';

@Component({
  selector: 'app-signos',
  templateUrl: './signos.component.html',
  styleUrls: ['./signos.component.css']
})

export class SignosComponent implements OnInit {

  displayedColumns = ['id', 'paciente', 'fecha', 'temperatura', 'pulso','ritmo','acciones'];
  dataSource: MatTableDataSource<Signos>;
  @ViewChild(MatPaginator) paginator: MatPaginator;
  @ViewChild(MatSort) sort: MatSort;

  signos: Signos[] = [];
  
  mensaje: string;

  idSignosSeleccionada: number;

  constructor(private signosService: SignosService, public route: ActivatedRoute, private snackBar: MatSnackBar) { }

  ngOnInit() {    
      this.signosService.signosCambio.subscribe(data => {
      this.dataSource = new MatTableDataSource(data);
      this.dataSource.paginator = this.paginator;
      this.dataSource.sort = this.sort;
    });

    this.signosService.listar().subscribe(data => {
      this.dataSource = new MatTableDataSource(data);
      this.dataSource.paginator = this.paginator;
      this.dataSource.sort = this.sort;
    });    
  }

  applyFilter(filterValue: string) {
    filterValue = filterValue.trim(); 
    filterValue = filterValue.toLowerCase(); 
    this.dataSource.filter = filterValue;
  }
  
  listarSignos() {
    this.signosService.listar().subscribe(data => {
      this.signos = data;
    });
  }
 
  eliminar(idSigno: number) {
    this.signosService.eliminar(idSigno).subscribe(data => {
      this.signosService.listar().subscribe(data => {
        this.signosService.signosCambio.next(data);
        this.signosService.mensajeCambio.next('Se eliminó');
      });
    });
  }

  limpiarControles() {
    this.signos = [];
    this.mensaje = '';
  }

}

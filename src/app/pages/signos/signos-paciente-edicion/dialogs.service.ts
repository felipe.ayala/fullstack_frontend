import { Injectable } from '@angular/core';
import { Observable } from 'rxjs/Observable';
import { SignosPacienteEdicionComponent } from '../../signos/signos-paciente-edicion/signos-paciente-edicion.component';
import { MatDialogRef, MatDialog } from '@angular/material';

@Injectable()
export class DialogsService {

    constructor(private dialog: MatDialog) { }

    public confirm(title: string, message: string): Observable<boolean> {

        console.log("10");
        let dialogRef: MatDialogRef<SignosPacienteEdicionComponent>;

        dialogRef = this.dialog.open(SignosPacienteEdicionComponent);

        //dialogRef.componentInstance.title = title;
        //dialogRef.componentInstance.message = message;

        return dialogRef.afterClosed();
    }
}
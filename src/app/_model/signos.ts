
import { Paciente } from "./paciente";

export class Signos{
    public idSigno: number;
    public temperatura: String;
    public pulso: String;
    public fecha: String;
    public ritmo: String;
    public paciente: Paciente;
}
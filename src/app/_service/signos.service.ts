import { HttpClient } from '@angular/common/http';
import { HOST } from './../_shared/var.constant';
import { Subject } from 'rxjs';
import { Signos } from './../_model/signos';
import { Injectable } from '@angular/core';

@Injectable({
    providedIn: 'root'
})
export class SignosService {
    signosCambio = new Subject<Signos[]>();
    mensajeCambio = new Subject<string>();
    url: string = `${HOST}/signos`;
    constructor(private http: HttpClient) { }
    listar() {
        return this.http.get<Signos[]>(this.url);
    }
    listarSignosPorId(id: number) {
        return this.http.get<Signos>(`${this.url}/${id}`);
    }
    registrar(signos: Signos) {
        return this.http.post(this.url, signos);
    }
    modificar(signos: Signos) {
        return this.http.put(this.url, signos);
    }
    eliminar(id: number) {
        return this.http.delete(`${this.url}/${id}`);
    }
}
